import React, { Component } from 'react';
import { StyleSheet, View, Button } from 'react-native';

export default class HomeScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Button
                    title="Go to Details"
                    onPress={() => this.props.navigation.navigate('Details')}
                />
                <Button
                    title="Go to ActivityIndicator"
                    onPress={() => this.props.navigation.navigate('Activity')}
                />
                <Button
                    title="Go to Button"
                    onPress={() => this.props.navigation.navigate('Button')}
                />
                <Button
                    title="Go to Image"
                    onPress={() => this.props.navigation.navigate('Image')}
                />
                <Button
                    title="Go to Keyboard"
                    onPress={() => this.props.navigation.navigate('Keyboard')}
                />
                <Button
                    title="Go to Modal"
                    onPress={() => this.props.navigation.navigate('Modal')}
                />
                <Button
                    title="Go to Picker"
                    onPress={() => this.props.navigation.navigate('Picker')}
                />
                <Button
                    title="Go to Progress Bar"
                    onPress={() => this.props.navigation.navigate('Progress')}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});