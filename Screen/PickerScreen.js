import React, { Component } from 'react';
import { Text, View, StyleSheet, Picker, } from 'react-native';


export default class PickerScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      state: 'Java',
    }
  }
  
  pickerItems = (max) => {
    for(let i = 1; i<max+1;i++) 
    return( <Picker.Item label={i} value={i} />)
  }

  render() {
    
    return (
      <View style={styles.container}>
        <Text style={styles.paragraph}>
          Change code in the editor and watch it change on your phone!
          Save to get a shareable url. You get a new url each time you save.
           {this.state.language}
        </Text> 
        <View style={{flexDirection:'row'}} >
        <Picker
          style={{width: 100}}
          selectedValue={this.state.language}
          onValueChange={(lang) => this.setState({language: lang})}> 
          {
            this.pickerItems(24)
          }
        </Picker>
        <Picker
          style={{width: 100}}
          selectedValue={this.state.language}
          onValueChange={(lang) => this.setState({language: lang})}> 
          {
            this.pickerItems(60)
          }
        </Picker>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
});
