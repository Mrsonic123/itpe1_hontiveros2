import { StyleSheet, View, Button } from 'react-native';
import React, { Component } from 'react';


export default class ButtonScreen extends Component {
    render() {
        return (
            <Button
                title="Learn More"
                color="#841584"
                accessibilityLabel="Learn more about this purple button"
            />
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
    }
})
